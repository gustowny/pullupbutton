//
// Libary for Arduino pullup button.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#ifndef _PULLUPBUTTON_H_
#define _PULLUPBUTTON_H_

#include "Arduino.h"
#include "buttonevent.h"
#include "localtimeobject.h"

// PullupButton scans a button connected between GND and a pullup input pin:
//                  ____
//   pin |----------o  o-----------| GND
//
class PullupButton : public LocalTimeObject
{
   public:

      // Constructor. Call the begin() member function to fully initialize!
      PullupButton();
      
      // Initializes the PullupButton.
      void begin(int pin);
      
      // Scans the button pin and returns a button event:
      //  None    : none of the situations below
      //  Pressed : the button is pressed down
      //  Clicked : the button was released after have beeing pressed
      //  HoldDown: the button has been pressed down > 1 second
      ButtonEvent scan();
               
   private:

      typedef enum {NotPressedState, PressedState, HoldState} ButtonStateType;

      const unsigned long minimumHoldTime = 1000ul;
      
      int               m_buttonPin;
      unsigned long     m_pressedTime;
      ButtonStateType   m_buttonState;
      unsigned long     m_localTime;
      unsigned long     m_lastSystemTime;
      
};

#endif // _PULLUPBUTTON_H_