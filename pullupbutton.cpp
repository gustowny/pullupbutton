//
// Libary for Arduino pullup button.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#include "pullupbutton.h"

//--------------------------------------------------------------------------

PullupButton::PullupButton() :
   m_buttonPin(-1),
   m_pressedTime(0),
   m_buttonState(NotPressedState),
   m_localTime(0),
   m_lastSystemTime(0)
{

}

//--------------------------------------------------------------------------

void PullupButton::begin(int pin)
{
   m_buttonPin = pin;

   pinMode(m_buttonPin, INPUT_PULLUP);
}

//--------------------------------------------------------------------------

ButtonEvent PullupButton::scan()
{
   syncLocalTime(); // sync local time with system time
   
   const bool  buttonPressed = (digitalRead(m_buttonPin) == LOW);
   ButtonEvent buttonEvent   = None;	  

   switch (m_buttonState)
   {
      case NotPressedState:
      {         
         if (buttonPressed)
         {
            m_pressedTime = localTime();
            m_buttonState = PressedState;
         }
         else
         {
            resetLocalTime();  // to avoid clock overflow
         }
      }
      break;
      
      case PressedState:
      {
         if (buttonPressed && ((localTime() - m_pressedTime) > minimumHoldTime))
         {
            buttonEvent   = HoldDown;
            m_buttonState = HoldState;
         }
         else if (buttonPressed)
         {
            buttonEvent   = Pressed;
         }
         else // button released
         {
            buttonEvent   = Clicked;
            m_buttonState = NotPressedState;
         }
      }
      break;
         
      case HoldState:
      {
         if (buttonPressed)
         {
            buttonEvent = HoldDown;
         }
         else 
         {
            m_buttonState = NotPressedState;
         }
      }
      break;
      
   }
      
   return buttonEvent;
}

