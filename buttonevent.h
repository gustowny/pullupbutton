//
// Definition of button events
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#ifndef _BUTTONEVENT_H_
#define _BUTTONEVENT_H_

typedef enum {None,     // None of the situations below
              Pressed,  // The button is pressed down
              Clicked,  // The button was released after have beeing pressed
              HoldDown  // The button has been pressed down > 1 second
             } ButtonEvent;

#endif // _BUTTONEVENT_H_